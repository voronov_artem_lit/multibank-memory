MAX_banks: 5  MAX_requests: 6 


~~~~~~~~~SUBTEST#0: REQ 1, BANK 1  | tick_cur[17]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2], WRITE_ALL[1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 1.000000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 1.000000 [Tr/clock]



~~~~~~~~~SUBTEST#1: REQ 2, BANK 1  | tick_cur[39]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.25], WRITE_ALL[1.2]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.100000 velocity: 0.714286 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.400000 velocity: 0.714286 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.100000 velocity: 0.714286 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.300000 velocity: 0.714286 [Tr/clock]



~~~~~~~~~SUBTEST#2: REQ 3, BANK 1  | tick_cur[65]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.7], WRITE_ALL[1.6]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.700000 velocity: 0.555556 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.800000 velocity: 0.555556 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.600000 velocity: 0.555556 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.400000 velocity: 0.555556 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.600000 velocity: 0.555556 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.800000 velocity: 0.555556 [Tr/clock]



~~~~~~~~~SUBTEST#3: REQ 4, BANK 1  | tick_cur[97]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.925], WRITE_ALL[2.05]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.600000 velocity: 0.416667 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 3.000000 velocity: 0.416667 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.900000 velocity: 0.416667 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.200000 velocity: 0.416667 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.800000 velocity: 0.416667 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.300000 velocity: 0.416667 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.700000 velocity: 0.416667 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.400000 velocity: 0.416667 [Tr/clock]



~~~~~~~~~SUBTEST#4: REQ 5, BANK 1  | tick_cur[139]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[3.74], WRITE_ALL[2.66]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.400000 velocity: 0.294118 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 4.200000 velocity: 0.294118 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 2.800000 velocity: 0.294118 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.900000 velocity: 0.294118 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 4.400000 velocity: 0.294118 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.294118 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.900000 velocity: 0.294118 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 3.000000 velocity: 0.294118 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.800000 velocity: 0.294118 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.500000 velocity: 0.294118 [Tr/clock]



~~~~~~~~~SUBTEST#5: REQ 6, BANK 1  | tick_cur[183]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[4.15], WRITE_ALL[3.08333]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 4.000000 velocity: 0.277778 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 3.700000 velocity: 0.277778 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 4.500000 velocity: 0.277778 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 4.200000 velocity: 0.277778 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 3     max_delay: 7     avr_delay: 3.900000 velocity: 0.277778 [Tr/clock]
READ  port[5]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 4.600000 velocity: 0.277778 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.300000 velocity: 0.277778 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.200000 velocity: 0.277778 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.300000 velocity: 0.277778 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.200000 velocity: 0.277778 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.000000 velocity: 0.277778 [Tr/clock]
WRITE port[5]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.500000 velocity: 0.277778 [Tr/clock]



~~~~~~~~~SUBTEST#6: REQ 1, BANK 2  | tick_cur[201]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2], WRITE_ALL[1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 1.000000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 1.000000 [Tr/clock]



~~~~~~~~~SUBTEST#7: REQ 2, BANK 2  | tick_cur[223]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.3], WRITE_ALL[1.15]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.200000 velocity: 0.714286 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.400000 velocity: 0.714286 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 0.714286 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.300000 velocity: 0.714286 [Tr/clock]



~~~~~~~~~SUBTEST#8: REQ 3, BANK 2  | tick_cur[251]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.8], WRITE_ALL[1.7]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.600000 velocity: 0.500000 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.800000 velocity: 0.500000 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 3.000000 velocity: 0.500000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.600000 velocity: 0.500000 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.600000 velocity: 0.500000 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.900000 velocity: 0.500000 [Tr/clock]



~~~~~~~~~SUBTEST#9: REQ 4, BANK 2  | tick_cur[285]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[3.1], WRITE_ALL[2.075]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.500000 velocity: 0.384615 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.400000 velocity: 0.384615 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.900000 velocity: 0.384615 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.600000 velocity: 0.384615 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.900000 velocity: 0.384615 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.400000 velocity: 0.384615 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.384615 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.900000 velocity: 0.384615 [Tr/clock]



~~~~~~~~~SUBTEST#10: REQ 5, BANK 2  | tick_cur[323]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[3.32], WRITE_ALL[2.62]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.400000 velocity: 0.333333 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.300000 velocity: 0.333333 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.400000 velocity: 0.333333 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.700000 velocity: 0.333333 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.800000 velocity: 0.333333 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 3.000000 velocity: 0.333333 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 2.100000 velocity: 0.333333 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.600000 velocity: 0.333333 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.900000 velocity: 0.333333 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.500000 velocity: 0.333333 [Tr/clock]



~~~~~~~~~SUBTEST#11: REQ 6, BANK 2  | tick_cur[363]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[3.76667], WRITE_ALL[2.91667]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 3.700000 velocity: 0.312500 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 4.000000 velocity: 0.312500 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.700000 velocity: 0.312500 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 4.100000 velocity: 0.312500 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 3     max_delay: 6     avr_delay: 3.600000 velocity: 0.312500 [Tr/clock]
READ  port[5]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.500000 velocity: 0.312500 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.200000 velocity: 0.312500 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.300000 velocity: 0.312500 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.100000 velocity: 0.312500 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.700000 velocity: 0.312500 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.000000 velocity: 0.312500 [Tr/clock]
WRITE port[5]   - count: 10      min_delay: 1     max_delay: 6     avr_delay: 3.200000 velocity: 0.312500 [Tr/clock]



~~~~~~~~~SUBTEST#12: REQ 1, BANK 3  | tick_cur[381]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2], WRITE_ALL[1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 1.000000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 1.000000 [Tr/clock]



~~~~~~~~~SUBTEST#13: REQ 2, BANK 3  | tick_cur[403]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.1], WRITE_ALL[1.2]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 0.714286 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.200000 velocity: 0.714286 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 0.714286 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.400000 velocity: 0.714286 [Tr/clock]



~~~~~~~~~SUBTEST#14: REQ 3, BANK 3  | tick_cur[430]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.8], WRITE_ALL[1.36667]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.600000 velocity: 0.526316 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.900000 velocity: 0.526316 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.900000 velocity: 0.526316 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.300000 velocity: 0.526316 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.400000 velocity: 0.526316 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.400000 velocity: 0.526316 [Tr/clock]



~~~~~~~~~SUBTEST#15: REQ 4, BANK 3  | tick_cur[463]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.75], WRITE_ALL[2.1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.500000 velocity: 0.400000 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.900000 velocity: 0.400000 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.800000 velocity: 0.400000 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.800000 velocity: 0.400000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.000000 velocity: 0.400000 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.400000 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.800000 velocity: 0.400000 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.500000 velocity: 0.400000 [Tr/clock]



~~~~~~~~~SUBTEST#16: REQ 5, BANK 3  | tick_cur[498]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[3.06], WRITE_ALL[2.28]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.100000 velocity: 0.370370 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.400000 velocity: 0.370370 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 2.900000 velocity: 0.370370 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.300000 velocity: 0.370370 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.600000 velocity: 0.370370 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.000000 velocity: 0.370370 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.700000 velocity: 0.370370 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 1.800000 velocity: 0.370370 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.200000 velocity: 0.370370 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.700000 velocity: 0.370370 [Tr/clock]



~~~~~~~~~SUBTEST#17: REQ 6, BANK 3  | tick_cur[531]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[3.15], WRITE_ALL[2.25]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.400000 velocity: 0.400000 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.500000 velocity: 0.400000 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 7     avr_delay: 2.900000 velocity: 0.400000 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 2.900000 velocity: 0.400000 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.000000 velocity: 0.400000 [Tr/clock]
READ  port[5]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.200000 velocity: 0.400000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.400000 velocity: 0.400000 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.300000 velocity: 0.400000 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.700000 velocity: 0.400000 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.500000 velocity: 0.400000 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 5     avr_delay: 2.500000 velocity: 0.400000 [Tr/clock]
WRITE port[5]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.400000 [Tr/clock]



~~~~~~~~~SUBTEST#18: REQ 1, BANK 4  | tick_cur[549]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2], WRITE_ALL[1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 1.000000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 1.000000 [Tr/clock]



~~~~~~~~~SUBTEST#19: REQ 2, BANK 4  | tick_cur[570]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.15], WRITE_ALL[1.25]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.100000 velocity: 0.769231 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.200000 velocity: 0.769231 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.200000 velocity: 0.769231 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.300000 velocity: 0.769231 [Tr/clock]



~~~~~~~~~SUBTEST#20: REQ 3, BANK 4  | tick_cur[593]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.46667], WRITE_ALL[1.33333]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.400000 velocity: 0.666667 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.500000 velocity: 0.666667 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.500000 velocity: 0.666667 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.300000 velocity: 0.666667 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.300000 velocity: 0.666667 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.400000 velocity: 0.666667 [Tr/clock]



~~~~~~~~~SUBTEST#21: REQ 4, BANK 4  | tick_cur[617]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.425], WRITE_ALL[1.475]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.300000 velocity: 0.625000 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.500000 velocity: 0.625000 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.600000 velocity: 0.625000 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.300000 velocity: 0.625000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.400000 velocity: 0.625000 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.400000 velocity: 0.625000 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.600000 velocity: 0.625000 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.500000 velocity: 0.625000 [Tr/clock]



~~~~~~~~~SUBTEST#22: REQ 5, BANK 4  | tick_cur[647]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.62], WRITE_ALL[1.84]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.900000 velocity: 0.454545 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 0.454545 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.600000 velocity: 0.454545 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.700000 velocity: 0.454545 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.900000 velocity: 0.454545 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.454545 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.400000 velocity: 0.454545 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.400000 velocity: 0.454545 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 2.100000 velocity: 0.454545 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 2.200000 velocity: 0.454545 [Tr/clock]



~~~~~~~~~SUBTEST#23: REQ 6, BANK 4  | tick_cur[676]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.65], WRITE_ALL[1.71667]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.300000 velocity: 0.476190 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.800000 velocity: 0.476190 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.300000 velocity: 0.476190 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.000000 velocity: 0.476190 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.400000 velocity: 0.476190 [Tr/clock]
READ  port[5]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.100000 velocity: 0.476190 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.300000 velocity: 0.476190 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.900000 velocity: 0.476190 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.476190 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.200000 velocity: 0.476190 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 2.000000 velocity: 0.476190 [Tr/clock]
WRITE port[5]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.800000 velocity: 0.476190 [Tr/clock]



~~~~~~~~~SUBTEST#24: REQ 1, BANK 5  | tick_cur[694]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2], WRITE_ALL[1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 1.000000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 1.000000 [Tr/clock]



~~~~~~~~~SUBTEST#25: REQ 2, BANK 5  | tick_cur[714]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.1], WRITE_ALL[1.1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 0.833333 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.200000 velocity: 0.833333 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 1     avr_delay: 1.000000 velocity: 0.833333 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.200000 velocity: 0.833333 [Tr/clock]



~~~~~~~~~SUBTEST#26: REQ 3, BANK 5  | tick_cur[738]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.23333], WRITE_ALL[1.33333]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 2     avr_delay: 2.000000 velocity: 0.625000 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.300000 velocity: 0.625000 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.400000 velocity: 0.625000 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.100000 velocity: 0.625000 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.300000 velocity: 0.625000 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.600000 velocity: 0.625000 [Tr/clock]



~~~~~~~~~SUBTEST#27: REQ 4, BANK 5  | tick_cur[763]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.375], WRITE_ALL[1.425]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.300000 velocity: 0.588235 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.400000 velocity: 0.588235 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 3     avr_delay: 2.400000 velocity: 0.588235 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.400000 velocity: 0.588235 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.100000 velocity: 0.588235 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.500000 velocity: 0.588235 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 2     avr_delay: 1.400000 velocity: 0.588235 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.700000 velocity: 0.588235 [Tr/clock]



~~~~~~~~~SUBTEST#28: REQ 5, BANK 5  | tick_cur[789]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.4], WRITE_ALL[1.58]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.500000 velocity: 0.555556 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.400000 velocity: 0.555556 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.500000 velocity: 0.555556 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.400000 velocity: 0.555556 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 4     avr_delay: 2.200000 velocity: 0.555556 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.500000 velocity: 0.555556 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.700000 velocity: 0.555556 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.300000 velocity: 0.555556 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.800000 velocity: 0.555556 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.600000 velocity: 0.555556 [Tr/clock]



~~~~~~~~~SUBTEST#29: REQ 6, BANK 5  | tick_cur[821]~~~~~~~~~~
         PARAM: Dt[100], Sc[0], Ac[50], AV_CNT[10]
         G_AV_DELAY: READ_ALL[2.96667], WRITE_ALL[1.85]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

READ  port[0]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 2.900000 velocity: 0.416667 [Tr/clock]
READ  port[1]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.400000 velocity: 0.416667 [Tr/clock]
READ  port[2]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.200000 velocity: 0.416667 [Tr/clock]
READ  port[3]   - count: 10      min_delay: 2     max_delay: 6     avr_delay: 3.200000 velocity: 0.416667 [Tr/clock]
READ  port[4]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 2.700000 velocity: 0.416667 [Tr/clock]
READ  port[5]   - count: 10      min_delay: 2     max_delay: 5     avr_delay: 3.400000 velocity: 0.416667 [Tr/clock]


WRITE port[0]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.000000 velocity: 0.416667 [Tr/clock]
WRITE port[1]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 1.900000 velocity: 0.416667 [Tr/clock]
WRITE port[2]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.600000 velocity: 0.416667 [Tr/clock]
WRITE port[3]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.800000 velocity: 0.416667 [Tr/clock]
WRITE port[4]   - count: 10      min_delay: 1     max_delay: 3     avr_delay: 1.700000 velocity: 0.416667 [Tr/clock]
WRITE port[5]   - count: 10      min_delay: 1     max_delay: 4     avr_delay: 2.100000 velocity: 0.416667 [Tr/clock]



